using Navigation;
using UnityEngine;

namespace View.Map.Scriptables
{
	public abstract class AbstractMapGenerator : ScriptableObject
	{
		#region PublicMethods

		public abstract Grid<NodeInfo> GetMapData();

		#endregion
	}
}
