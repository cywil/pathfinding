using Navigation;
using UnityEngine;

namespace View.Map.Scriptables
{
	[CreateAssetMenu(fileName = "RandomMapGenerator", menuName = "Map/RandomMapGenerator", order = 1)]
	public sealed class RandomMapGenerator : AbstractMapGenerator
	{
		#region PublicFields

		public Vector2Int StartPosition => m_startPosition;

		#endregion

		#region SerializeFields

		[SerializeField] private int m_width;
		[SerializeField] private int m_height;
		[SerializeField] private Vector2Int m_startPosition;
		[Range(0, 1f)] [SerializeField] private float m_unwakableInPercent;

		#endregion

		#region PrivateFields

		private Grid<NodeInfo> m_mapTileData;

		#endregion

		#region PublicMethods

		public override Grid<NodeInfo> GetMapData()
		{
			return new Grid<NodeInfo>(m_width, m_height, position => new NodeInfo(position, position == m_startPosition || Random.value > m_unwakableInPercent));
		}

		#endregion
	}
}
