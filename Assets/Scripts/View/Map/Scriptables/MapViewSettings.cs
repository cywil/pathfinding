using UnityEngine;

namespace View.Map.Scriptables
{
	[CreateAssetMenu(fileName = "MapViewSettings", menuName = "Map/MapViewSettings", order = 1)]
	public sealed class MapViewSettings : ScriptableObject
	{
		#region PublicFields

		[Header("Path")] public bool SimulatePathProgress;

		[Tooltip("Interval in seconds.")] public float SimulationInterval;

		[Header("Tiles")] public float TileSize;
		public TileViewSettings TileViewSettings;

		#endregion
	}
}
