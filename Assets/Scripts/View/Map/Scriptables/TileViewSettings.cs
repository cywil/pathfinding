using UnityEngine;

namespace View.Map.Scriptables
{
	[CreateAssetMenu(fileName = "TileViewSettings", menuName = "Map/TileViewSettings", order = 1)]
	public sealed class TileViewSettings : ScriptableObject
	{
		#region PublicFields

		public string TilePrefabAdress;

		[Header("Colors")] public float ChangeColorSpeed;
		public Color InactiveTileColor;
		public Color ActiveTileColor;

		#endregion
	}
}
