using Commands;
using UnityEngine;

namespace View.Map.Commands
{
	public sealed class MoveToPositionCommand : ICommand
	{
		#region PrivateFields

		private MapController m_mapController;
		private Vector2Int m_position;

		#endregion

		#region Constructors

		public MoveToPositionCommand(MapController mapController, Vector2Int position)
		{
			m_mapController = mapController;
			m_position = position;
		}

		#endregion

		#region InterfaceImplementations

		public void Invoke()
		{
			m_mapController.MoveToPosition(m_position);
		}

		#endregion
	}
}
