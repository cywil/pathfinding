using System.Collections;
using System.Threading.Tasks;
using Commands;
using Navigation;
using Navigation.Scriptables;
using UnityEngine;
using Utils;
using View.Map.Commands;
using View.Map.Scriptables;

namespace View.Map
{
	public sealed class MapController : MonoBehaviour
	{
		#region SerializeFields

		[Header("Data")] [SerializeField] private RandomMapGenerator m_mapGenerator;
		[SerializeField] private MapViewSettings m_mapViewSettings;
		[SerializeField] private AbstractPathfinding m_pathfinding;

		[Header("View")] [SerializeField] private MapView m_mapView;

		#endregion

		#region PrivateFields

		private Dispatcher m_dispatcher;
		private Coroutine m_pathCoroutine;
		private Grid<NodeInfo> m_mapData;
		private CommandBuffer m_moveCommandBuffer;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			m_dispatcher = Dispatcher.Instance;
			m_moveCommandBuffer = new CommandBuffer();

			m_mapData = m_mapGenerator.GetMapData();

			m_pathfinding = m_pathfinding.CreateInstance();
			m_pathfinding.Initialize(m_mapData);

			m_mapView.Initialize(m_mapViewSettings, m_mapData, m_mapGenerator.StartPosition);
			m_mapView.PatchRequested += OnPatchRequested;
		}

		private void OnDestroy()
		{
			if (m_mapView != null)
			{
				m_mapView.PatchRequested -= OnPatchRequested;
			}
		}

		#endregion

		#region PublicMethods

		public void MoveToPosition(Vector2Int position)
		{
			m_mapView.SetCurrentTile(position);
		}

		#endregion

		#region PrivateMethods

		private void OnPatchRequested(Vector2Int start, Vector2Int end)
		{
			RequestPath(start, end);
		}

		private async void RequestPath(Vector2Int start, Vector2Int end)
		{
			await Task.Factory.StartNew(
				() =>
				{
					m_pathfinding.CalculatePath(
						start,
						end,
						result => { m_dispatcher.Add(() => OnPathResult(result)); });
				});
		}

		private void OnPathResult(Vector2Int[] path)
		{
			if (path.Length == 0)
			{
				Debug.Log("Can't find path");
				return;
			}

			if (m_pathCoroutine != null)
			{
				StopCoroutine(m_pathCoroutine);
			}

			m_moveCommandBuffer.ClearBuffer(0);

			if (!m_mapViewSettings.SimulatePathProgress)
			{
				m_moveCommandBuffer.AddCommand(new MoveToPositionCommand(this, path[path.Length - 1]));
			}
			else
			{
				foreach (Vector2Int position in path)
				{
					m_moveCommandBuffer.AddCommand(new MoveToPositionCommand(this, position));
				}
			}

			m_pathCoroutine = StartCoroutine(PathProgressCoroutine(path));
		}

		private IEnumerator PathProgressCoroutine(Vector2Int[] path)
		{
			while (m_moveCommandBuffer.BufferLength > 0)
			{
				m_moveCommandBuffer.InvokeFirst(true);
				yield return new WaitForSeconds(m_mapViewSettings.SimulationInterval);
			}
		}

		#endregion
	}
}
