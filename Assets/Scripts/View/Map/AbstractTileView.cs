using System;
using UnityEngine;
using UnityEngine.EventSystems;
using View.Map.Scriptables;

namespace View.Map
{
	public abstract class AbstractTileView : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
	{
		#region Constants

		private const float FADE_UPDATE_TOLERANCE = .001f;

		#endregion

		#region PublicFields

		public Vector2Int Position { get; private set; }

		public bool IsActiveTile
		{
			get => m_isActiveTile;

			set
			{
				m_isActiveTile = value;
				ShowRollover = false;
				m_fadeTarget = value ? 1 : 0;
				m_foot.gameObject.SetActive(m_isActiveTile);
			}
		}

		#endregion

		#region EventsAndDelegates

		public event Action<AbstractTileView> Clicked;

		#endregion

		#region SerializeFields

		[SerializeField] private SpriteRenderer m_tile;
		[SerializeField] private SpriteRenderer m_foot;
		[SerializeField] private SpriteRenderer m_targetIcon;

		#endregion

		#region PrivateFields

		private bool ShowRollover
		{
			set => m_targetIcon.gameObject.SetActive(value);
		}

		private float m_fade;
		private float m_fadeTarget;

		private bool m_isActiveTile;
		private TileViewSettings m_tileViewSettings;

		#endregion

		#region UnityMethods

		private void Update()
		{
			UpdateFading();
		}

		#endregion

		#region InterfaceImplementations

		public void OnPointerClick(PointerEventData eventData)
		{
			if (IsActiveTile)
			{
				return;
			}

			Clicked?.Invoke(this);
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (!m_isActiveTile)
			{
				ShowRollover = true;
			}
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			ShowRollover = false;
		}

		#endregion

		#region PublicMethods

		public void Initialize(Vector2Int position, float tileSize, TileViewSettings tileViewSettings)
		{
			m_tileViewSettings = tileViewSettings;
			Position = position;

			transform.localScale = Vector3.one * tileSize;
			transform.localPosition = new Vector3(Position.x * tileSize, Position.y * tileSize, 0);

			m_tile.color = m_tileViewSettings.InactiveTileColor;
			IsActiveTile = false;
		}

		#endregion

		#region PrivateMethods

		private void UpdateFading()
		{
			if (Math.Abs(m_fade - m_fadeTarget) < FADE_UPDATE_TOLERANCE)
			{
				return;
			}

			m_fade = Mathf.Lerp(m_fade, m_fadeTarget, Time.deltaTime * m_tileViewSettings.ChangeColorSpeed);
			m_tile.color = Color.Lerp(m_tileViewSettings.InactiveTileColor, m_tileViewSettings.ActiveTileColor, m_fade);
		}

		#endregion
	}
}
