using System;
using System.Collections.Generic;
using Navigation;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using View.Map.Scriptables;

namespace View.Map
{
	public sealed class MapView : MonoBehaviour
	{
		#region EventsAndDelegates

		public event Action<Vector2Int, Vector2Int> PatchRequested;

		#endregion

		#region PrivateFields

		private List<AbstractTileView> m_tiles;
		private Grid<AbstractTileView> m_grid;
		private AbstractTileView m_currentTile;
		private MapViewSettings m_mapViewSettings;
		private bool m_initialized;
		private Grid<NodeInfo> m_mapData;
		private AbstractTileView m_tilePrefab;

		#endregion

		#region UnityMethods

		private void OnDestroy()
		{
			if (m_tiles != null)
			{
				foreach (AbstractTileView tile in m_tiles)
				{
					tile.Clicked -= OnTileClicked;
				}

				m_tiles.Clear();
			}
		}

		#endregion

		#region PublicMethods

		public void Initialize(MapViewSettings mapViewSettings, Grid<NodeInfo> mapData, Vector2Int startPosition)
		{
			if (m_initialized)
			{
				return;
			}

			m_initialized = true;
			m_mapViewSettings = mapViewSettings;
			m_mapData = mapData;

			AsyncOperationHandle<GameObject> loadAssets = Addressables.LoadAssetAsync<GameObject>(m_mapViewSettings.TileViewSettings.TilePrefabAdress);

			loadAssets.Completed += handle =>
			{
				m_tilePrefab = handle.Result.GetComponent<AbstractTileView>();
				Generate(m_mapData, startPosition);
			};
		}

		public void SetCurrentTile(Vector2Int position)
		{
			if (m_currentTile != null)
			{
				m_currentTile.IsActiveTile = false;
			}

			m_currentTile = m_grid.GetNode(position);
			m_currentTile.IsActiveTile = true;
		}

		#endregion

		#region PrivateMethods

		private void Generate(Grid<NodeInfo> mapData, Vector2Int startPosition)
		{
			float tileSize = m_mapViewSettings.TileSize;
			int width = mapData.Width;
			int height = mapData.Height;

			transform.localPosition = new Vector3(-width / 2f * tileSize + tileSize / 2f, -height * tileSize / 2f + tileSize / 2f, 0);

			if (m_tiles == null)
			{
				m_tiles = new List<AbstractTileView>();
			}

			m_grid = new Grid<AbstractTileView>(width, height, CreateTile);

			m_currentTile = m_grid.GetNode(startPosition);
			m_currentTile.IsActiveTile = true;
		}

		private AbstractTileView CreateTile(Vector2Int position)
		{
			NodeInfo node = m_mapData.GetNode(position);

			if (!node.Walkable)
			{
				return null;
			}

			AbstractTileView tile = Instantiate(m_tilePrefab, transform);

			tile.Initialize(node.Position, m_mapViewSettings.TileSize, m_mapViewSettings.TileViewSettings);
			tile.Clicked += OnTileClicked;

			m_tiles.Add(tile);

			return tile;
		}

		private void OnTileClicked(AbstractTileView tile)
		{
			if (m_currentTile == null)
			{
				return;
			}

			PatchRequested?.Invoke(m_currentTile.Position, tile.Position);
		}

		#endregion
	}
}
