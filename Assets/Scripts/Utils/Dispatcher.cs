using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
	public sealed class Dispatcher : MonoBehaviour
	{
		#region Statics

		private static Dispatcher m_instance;

		#endregion

		#region PublicFields

		public static Dispatcher Instance
		{
			get
			{
				if (m_instance == null)
				{
					new GameObject("Dispatcher").AddComponent<Dispatcher>();
				}

				return m_instance;
			}
		}

		#endregion

		#region PrivateFields

		private readonly List<Action> m_actions = new List<Action>();

		#endregion

		#region UnityMethods

		private void Awake()
		{
			if (m_instance != null && m_instance != this)
			{
				Destroy(gameObject);
				return;
			}

			m_instance = this;
			DontDestroyOnLoad(this);
		}

		private void LateUpdate()
		{
			lock (m_actions)
			{
				foreach (Action action in m_actions)
				{
					action?.Invoke();
				}

				m_actions.Clear();
			}
		}

		#endregion

		#region PublicMethods

		public void Add(Action action)
		{
			if (action != null)
			{
				lock (m_actions)
				{
					m_actions.Add(action);
				}
			}
		}

		#endregion
	}
}
