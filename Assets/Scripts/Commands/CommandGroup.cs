using System.Collections.Generic;

namespace Commands
{
	public sealed class CommandGroup : ICommand
	{
		#region PrivateFields

		private readonly ICollection<ICommand> m_commands;

		#endregion

		#region Constructors

		public CommandGroup()
		{
			m_commands = new List<ICommand>();
		}

		#endregion

		#region InterfaceImplementations

		public void Invoke()
		{
			foreach (ICommand command in m_commands)
			{
				command.Invoke();
			}
		}

		#endregion

		#region PublicMethods

		public CommandGroup Add(ICommand command)
		{
			if (command != null)
			{
				m_commands.Add(command);
			}

			return this;
		}

		#endregion
	}
}
