using System.Collections.Generic;
using UnityEngine;

namespace Commands
{
	public sealed class CommandBuffer
	{
		#region PublicFields

		public int BufferLength => m_buffer.Count;

		#endregion

		#region PrivateFields

		private readonly List<ICommand> m_buffer;

		#endregion

		#region Constructors

		public CommandBuffer()
		{
			m_buffer = new List<ICommand>();
		}

		#endregion

		#region PublicMethods

		public void AddCommand(ICommand command)
		{
			m_buffer.Add(command);
		}

		public void ClearBuffer(int startIndex)
		{
			if (startIndex > m_buffer.Count)
			{
				Debug.LogWarning("Start index out of buffer length.");
				return;
			}

			m_buffer.RemoveRange(startIndex, m_buffer.Count - startIndex);
		}

		public void InvokeBuffer()
		{
			foreach (ICommand command in m_buffer)
			{
				command?.Invoke();
			}
		}

		public void InvokeBuffer(int range)
		{
			if (range > m_buffer.Count - 1)
			{
				Debug.Log("Range out of buffer bounds.");
				return;
			}

			for (int i = 0; i <= range; i++)
			{
				m_buffer[i]?.Invoke();
			}
		}

		public void InvokeFirst(bool removeOnInvoke)
		{
			if (m_buffer.Count == 0)
			{
				return;
			}

			m_buffer[0]?.Invoke();

			if (removeOnInvoke)
			{
				m_buffer.RemoveAt(0);
			}
		}

		public void InvokeLast()
		{
			m_buffer[m_buffer.Count - 1]?.Invoke();
		}

		#endregion
	}
}
