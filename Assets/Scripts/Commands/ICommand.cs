﻿namespace Commands
{
	public interface ICommand
	{
		#region PublicMethods

		void Invoke();

		#endregion
	}
}
