using UnityEngine;

namespace Navigation.Scriptables
{
	[CreateAssetMenu(fileName = "PathfindingSettings", menuName = "Navigation/PathfindingSettings", order = 1)]
	public class PathfindingSettings : ScriptableObject
	{
		#region PublicFields

		public int StraightCost;
		public int DiagonalCost;
		public bool CheckDiagonal;

		#endregion
	}
}
