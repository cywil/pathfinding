using Navigation.Algorithms;
using Navigation.Interfaces;
using UnityEngine;

namespace Navigation.Scriptables
{
	[CreateAssetMenu(fileName = "AStarPathfinding", menuName = "Navigation/AStarPathfinding", order = 1)]
	public class AStarPathfinding : AbstractPathfinding
	{
		#region ProtectedMethods

		protected override IPathfindingAlgorithm CreatePathfindingAlgorithm(Grid<NodeInfo> mapData)
		{
			return new AStarPathfindingAlgorithm(mapData, m_settings);
		}

		#endregion
	}
}
