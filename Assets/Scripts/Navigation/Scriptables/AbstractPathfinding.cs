using System;
using Navigation.Interfaces;
using UnityEngine;

namespace Navigation.Scriptables
{
	public abstract class AbstractPathfinding : ScriptableObject
	{
		#region SerializeFields

		[SerializeField] protected PathfindingSettings m_settings;

		#endregion

		#region PrivateFields

		private IPathfindingAlgorithm m_pathfinding;

		#endregion

		#region PublicMethods

		public virtual void Initialize(Grid<NodeInfo> mapData)
		{
			m_pathfinding = CreatePathfindingAlgorithm(mapData);
		}

		public void CalculatePath(Vector2Int start, Vector2Int end, Action<Vector2Int[]> resultCallback)
		{
			m_pathfinding.CalculatePath(start, end, resultCallback);
		}

		public AbstractPathfinding CreateInstance()
		{
			return Instantiate(this);
		}

		#endregion

		#region ProtectedMethods

		protected abstract IPathfindingAlgorithm CreatePathfindingAlgorithm(Grid<NodeInfo> mapData);

		#endregion
	}
}
