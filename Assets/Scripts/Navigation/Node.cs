using System.Collections.Generic;

namespace Navigation
{
	public sealed class Node
	{
		#region PublicFields

		public NodeInfo NodeInfo { get; set; }
		public Node ParentNode { get; set; }
		public List<Node> Neighbours { get; set; }

		public int GCost { get; set; }
		public int HCost { get; set; }
		public int FCost => GCost + HCost;

		#endregion

		#region Constructors

		public Node(NodeInfo nodeInfo)
		{
			NodeInfo = nodeInfo;
		}

		#endregion
	}
}
