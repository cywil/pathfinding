using System.Collections.Generic;
using UnityEngine;

namespace Navigation
{
	public static class PathfindingUtils
	{
		#region PublicMethods

		public static int CalculateCost(int straightCost, int diagonalCost, Vector2Int startNodePosition, Vector2Int endNodePosition)
		{
			int xDist = Mathf.Abs(startNodePosition.x - endNodePosition.x);
			int yDist = Mathf.Abs(startNodePosition.y - endNodePosition.y);
			int remainDist = Mathf.Abs(xDist - yDist);

			return diagonalCost * Mathf.Min(xDist, yDist) + straightCost * remainDist;
		}

		public static Vector2Int[] RetracePath(Node endNode)
		{
			var nodes = new List<Node> { endNode };
			Node currentNode = endNode;

			while (currentNode.ParentNode != null)
			{
				nodes.Add(currentNode.ParentNode);
				currentNode = currentNode.ParentNode;
			}

			var positions = new Vector2Int[nodes.Count];

			for (int i = 0; i < positions.Length; i++)
			{
				positions[i] = nodes[nodes.Count - i - 1].NodeInfo.Position;
			}

			return positions;
		}

		public static Node GetLowestFCostNode(IReadOnlyList<Node> nodes)
		{
			Node lowestFCostNode = nodes[0];

			for (int i = 1; i < nodes.Count; i++)
			{
				Node node = nodes[i];

				if (node.FCost < lowestFCostNode.FCost)
				{
					lowestFCostNode = node;
				}
			}

			return lowestFCostNode;
		}

		#endregion
	}
}
