﻿using System;
using System.Collections.Generic;
using Navigation.Interfaces;
using Navigation.Scriptables;
using UnityEngine;

namespace Navigation.Algorithms
{
	public sealed class AStarPathfindingAlgorithm : IPathfindingAlgorithm
	{
		#region PrivateFields

		private Grid<Node> m_grid;
		private PathfindingSettings m_settings;

		#endregion

		#region Constructors

		public AStarPathfindingAlgorithm(Grid<NodeInfo> mapData, PathfindingSettings settings)
		{
			m_settings = settings;

			m_grid = new Grid<Node>(
				mapData.Width,
				mapData.Height,
				position => new Node(mapData.GetNode(position)));

			CacheNeighbours();
		}

		#endregion

		#region InterfaceImplementations

		public void CalculatePath(Vector2Int start, Vector2Int end, Action<Vector2Int[]> resultCallback)
		{
			var path = new Vector2Int[0];
			bool pathSuccess = false;

			Node startNode = m_grid.GetNode(start);
			Node endNode = m_grid.GetNode(end);

			startNode.ParentNode = startNode;

			if (!startNode.NodeInfo.Walkable || !endNode.NodeInfo.Walkable)
			{
				resultCallback?.Invoke(path);
				return;
			}

			var openSet = new List<Node> { startNode };
			var closedSet = new HashSet<Node>();

			foreach (Node node in m_grid)
			{
				node.GCost = int.MaxValue;
				node.ParentNode = null;
			}

			startNode.GCost = 0;
			startNode.HCost = CalculateCost(startNode.NodeInfo.Position, endNode.NodeInfo.Position);

			while (openSet.Count > 0)
			{
				Node currentNode = PathfindingUtils.GetLowestFCostNode(openSet);
				openSet.Remove(currentNode);
				closedSet.Add(currentNode);

				if (currentNode == endNode)
				{
					pathSuccess = true;
					break;
				}

				foreach (Node neighbour in currentNode.Neighbours)
				{
					if (!neighbour.NodeInfo.Walkable || closedSet.Contains(neighbour))
					{
						continue;
					}

					int gCost = currentNode.GCost + CalculateCost(currentNode.NodeInfo.Position, neighbour.NodeInfo.Position);

					if (gCost < neighbour.GCost)
					{
						neighbour.ParentNode = currentNode;
						neighbour.GCost = gCost;
						neighbour.HCost = CalculateCost(neighbour.NodeInfo.Position, endNode.NodeInfo.Position);
					}

					if (!openSet.Contains(neighbour))
					{
						openSet.Add(neighbour);
					}
				}
			}

			if (pathSuccess)
			{
				path = PathfindingUtils.RetracePath(endNode);
			}

			resultCallback?.Invoke(path);
		}

		#endregion

		#region PrivateMethods

		private void CacheNeighbours()
		{
			foreach (Node node in m_grid)
			{
				node.Neighbours = m_grid.GetNeighbours(node.NodeInfo.Position, m_settings.CheckDiagonal);
			}
		}

		private int CalculateCost(Vector2Int startNodePosition, Vector2Int endNodePosition)
		{
			return PathfindingUtils.CalculateCost(m_settings.StraightCost, m_settings.DiagonalCost, startNodePosition, endNodePosition);
		}

		#endregion
	}
}
