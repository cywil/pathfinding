using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Navigation
{
	public sealed class Grid<T> : IEnumerable<T>
	{
		#region PublicFields

		public int Width { get; }
		public int Height { get; }

		#endregion

		#region PrivateFields

		private T[,] m_grid;

		#endregion

		#region Constructors

		public Grid(int width, int height, Func<Vector2Int, T> nodeFactory)
		{
			Height = height;
			Width = width;

			m_grid = new T[Width, Height];

			if (nodeFactory == null)
			{
				throw new ArgumentNullException("Node factory is null.");
			}

			for (int i = 0; i < Width; i++)
			{
				for (int j = 0; j < Height; j++)
				{
					m_grid[i, j] = nodeFactory.Invoke(new Vector2Int(i, j));
				}
			}
		}

		#endregion

		#region InterfaceImplementations

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < m_grid.GetLength(0); i++)
			{
				for (int j = 0; j < m_grid.GetLength(1); j++)
				{
					yield return m_grid[i, j];
				}
			}
		}

		#endregion

		#region PublicMethods

		public List<T> GetNeighbours(Vector2Int nodePosition, bool checkDiagonal)
		{
			var neighbours = new List<T>();

			for (int x = -1; x <= 1; x++)
			{
				for (int y = -1; y <= 1; y++)
				{
					if (x == 0 && y == 0)
					{
						continue;
					}

					if (!checkDiagonal && Math.Abs(x) == Mathf.Abs(y))
					{
						continue;
					}

					int checkX = nodePosition.x + x;
					int checkY = nodePosition.y + y;

					if (checkX >= 0 && checkX < Width && checkY >= 0 && checkY < Height)
					{
						neighbours.Add(GetNode(new Vector2Int(checkX, checkY)));
					}
				}
			}

			return neighbours;
		}

		public T GetNode(Vector2Int position)
		{
			if (position.x < 0 || position.x > Width - 1 || position.y < 0 || position.y > Height - 1)
			{
				throw new ArgumentOutOfRangeException($"Postition {position} is out ouf grid.");
			}

			return m_grid[position.x, position.y];
		}

		#endregion
	}
}
