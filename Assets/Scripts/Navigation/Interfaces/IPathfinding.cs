using System;
using UnityEngine;

namespace Navigation.Interfaces
{
	public interface IPathfindingAlgorithm
	{
		#region PublicMethods

		void CalculatePath(Vector2Int start, Vector2Int end, Action<Vector2Int[]> resultCallback);

		#endregion
	}
}
