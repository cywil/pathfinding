using UnityEngine;

namespace Navigation
{
	public struct NodeInfo
	{
		public Vector2Int Position;
		public bool Walkable;

		public NodeInfo(Vector2Int position, bool walkable)
		{
			Position = position;
			Walkable = walkable;
		}
	}
}
